// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// Функція дозволяє один раз написати код, який виконує якусь дію, а не переписувати його багато разів, 
// і потім за потреби визивати цей код в будь-якому місці програми, і навіть підставляти різні аргументи.

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
// Функція повинна щось виконувати з чимось, тому аргументи це те, з чим вона працює та над чим вона виконує дію.

// 3. Що таке оператор return та як він працює всередині функції?
// Змінні, оголошені зсередини функції, назовні не видно, результати будь-яких внутрішніх дій теж не видно.
// Щоб отримати щось з функції, інформацію, певний результат, ми повинні використовувати return, 
// в якому ми вкажемо, що саме ми хочемо повернути.

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа.
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності:
// - Після введення даних додати перевірку їхньої коректності. 
// Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову 
// (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).

let a = +prompt("Enter first number"), b = +prompt("Enter second number");
console.log(a + " та " + b);
let operator = prompt("Enter one of following arithmetic operators: + , - , * , /");
console.log(operator);
let result = 0;

while (true) {
    if (isNaN(parseInt(a)) || isNaN(a)) {
        a = +prompt("Enter first number again");       
    }
    else if (isNaN(parseInt(b)) || isNaN(b)) {
        b = +prompt("Enter second number again");
    }
    else if (operator !== "+"
        && operator !== "-"
        && operator !== "*"
        && operator !== "/") {
        operator = prompt("Please, reenter corect operator: + , - , * , /");
    }
    else {
        break;
    }    
}

function arithm(a, operator, b) {
    switch (operator) {
        case "+":
            result = +a + +b;
            break;
        case "-":
            result = a - b;
            break;
        case "*":
            result = a * b;
            break;
        case "/":
            result = a / b;
            break;
    }
    return result;
}
arithm(a, operator, b);
console.log(`Робимо арифметичну дію ${a} ${operator} ${b}`);
console.log(`Отримуємо ${result}`);

// function arithm(a, operator, b) {
//     if (operator == "+") {
//         result = a + b;
//         return result;
//         console.log(result);
//     }
//     else if (operator == "-") result = a - b;
//     else if (operator == "*") result = a * b;
//     else result = a/b;
//     return result;
// }
// arithm(a, operator, b);
// console.log(result);


